// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
	  
	  
	  var UporabnikiTabela4 = besede[0].split(',')
	  UporabnikiTabela4[0] = UporabnikiTabela4[0].substr(1);
	  if(UporabnikiTabela4.length >= 2) {
	    UporabnikiTabela4[(UporabnikiTabela4.length)-1] = UporabnikiTabela4[(UporabnikiTabela4.length)-1].slice(0, -1);
	  }
	  
	  if(UporabnikiTabela4.length == 1) {
	    UporabnikiTabela4[0] = UporabnikiTabela4[0].slice(0, -1);
	  }
	  
      var besedilo = besede.join(' ');
	  
      for(var i in UporabnikiTabela4) {
		var parametri = besedilo.split('\"');
		if (parametri) {
			this.socket.emit('sporocilo', {vzdevek: UporabnikiTabela4[i], besedilo: parametri[3]});
			sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
		} else {
			sporocilo = 'Neznan ukaz';
		}
	  }
      break;
      //1111111111111111111111111111
    case 'barva':
      besede.shift();
      var keraBarva = besede.join(' ');
      var xBarva = document.getElementById('sporocila');
      xBarva.style.color = keraBarva;
      var yBarva = document.getElementById('kanal');
      yBarva.style.color = keraBarva;
    break;
    //1111111111111111111111111111111111111111
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};